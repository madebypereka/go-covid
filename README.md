# go-covid

Golang library for RESTFUL json

## Installation

Run the following to setup. Make sure [docker](https://www.docker.com/products/docker-desktop) is installed.

```bash
docker compose up --build
```

## Usage

1. Running API
```bash
See following [OpenAPI3](api/oas3.yaml) file.
```

2. Connecting to Postgres
```bash
$ docker exec -it go-covid-db psql -U postgres
$ psql
$ \c go-covid-db
```

## Setup
```bash
# run the query to simulate file uploads (use oas3.yaml file)
PUT https://{server}/api/uploads
```

1. Call will read file from `/resource/uploads/data_covid.csv`
2. Upload data to db for persistence (postgres)
3. Upon success, the file is moved from `resource/uploads/*` to `resource/archives/*`
4. Subsequent data will need similar formatted files to be placed within `resource/uploads` directory


