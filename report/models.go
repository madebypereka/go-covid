package report

import (
	"strings"

	"github.com/jinzhu/gorm"
)

type CleanString string

func (c CleanString) String() string {
	formatted := strings.TrimSpace(string(c))
	formatted = strings.ReplaceAll(formatted, " ", "_")
	return strings.ToLower(formatted)
}

type Report struct {
	gorm.Model
	Date            string `gorm:"Not Null" csv:"date"`
	State           string `csv:"state"`
	CasesNew        int    `csv:"cases_new"`
	CasesImport     int    `csv:"cases_import"`
	CasesRecovered  int    `csv:"cases_recovered"`
	CasesActive     int    `csv:"cases_active"`
	CasesCluster    int    `csv:"cases_cluster"`
	CasesUnvax      int    `csv:"cases_unvax"`
	CasesPvax       int    `csv:"cases_pvax"`
	CasesFvax       int    `csv:"cases_fvax"`
	CasesBoost      int    `csv:"cases_boost"`
	CasesChild      int    `csv:"cases_child"`
	CasesAdolescent int    `csv:"cases_adolescent"`
	CasesAdult      int    `csv:"cases_Adult"`
	CasesElderly    int    `csv:"cases_elderly"`
	Cases_0_4       int    `csv:"cases_0_4"`
	Cases_5_11      int    `csv:"cases_5_11"`
	Cases_12_17     int    `csv:"cases_12_17"`
	Cases_18_29     int    `csv:"cases_18_29"`
	Cases_30_39     int    `csv:"cases_30_39"`
	Cases_40_49     int    `csv:"cases_40_49"`
	Cases_50_59     int    `csv:"cases_50_59"`
	Cases_60_69     int    `csv:"cases_60_69"`
	Cases_70_79     int    `csv:"cases_70_79"`
	Cases_80        int    `csv:"cases_80"`
}

func (r *Report) sum() int {
	return r.CasesNew + r.CasesImport + r.CasesRecovered + r.CasesActive + r.CasesCluster +
		r.CasesUnvax + r.CasesActive + r.CasesPvax + r.CasesFvax + r.CasesBoost + r.CasesChild +
		r.CasesAdolescent + r.CasesAdult + r.CasesElderly +
		r.Cases_0_4 + r.Cases_5_11 + r.Cases_12_17 + r.Cases_18_29 + r.Cases_30_39 +
		r.Cases_40_49 + r.Cases_50_59 + r.Cases_60_69 + r.Cases_70_79 + r.Cases_80
}

func (r *Report) formatted() FormattedReport {
	cases := ReportCases{
		New: r.CasesNew, Import: r.CasesImport, Recovered: r.CasesRecovered, Active: r.CasesActive, Cluster: r.CasesCluster,
		Unvax: r.CasesUnvax, Pvax: r.CasesPvax, Fvax: r.CasesFvax, Boost: r.CasesBoost, Child: r.CasesChild,
		Adolescent: r.CasesAdolescent, Adult: r.CasesAdult, Elderly: r.CasesElderly,
		Cases04: r.Cases_0_4, Cases511: r.Cases_5_11, Cases1217: r.Cases_12_17, Cases1829: r.Cases_18_29, Cases3039: r.Cases_30_39,
		Cases4049: r.Cases_40_49, Cases5059: r.Cases_50_59, Cases6069: r.Cases_60_69, Cases7079: r.Cases_70_79, Cases80: r.Cases_80,
	}

	return FormattedReport{
		Date: r.Date, State: r.State,
		Cases: cases,
		Sum:   r.sum(),
	}
}

type ReportCases struct {
	New        int `json:"new"`
	Import     int `json:"import"`
	Recovered  int `json:"recovered"`
	Active     int `json:"active"`
	Cluster    int `json:"cluster"`
	Unvax      int `json:"unvax"`
	Pvax       int `json:"pvax"`
	Fvax       int `json:"fvax"`
	Boost      int `json:"boost"`
	Child      int `json:"child"`
	Adolescent int `json:"adolescent"`
	Adult      int `json:"adult"`
	Elderly    int `json:"elderly"`
	Cases04    int `json:"cases_0_4"`
	Cases511   int `json:"cases_5_11"`
	Cases1217  int `json:"cases_12_17"`
	Cases1829  int `json:"cases_18_29"`
	Cases3039  int `json:"cases_30_39"`
	Cases4049  int `json:"cases_40_49"`
	Cases5059  int `json:"cases_50_59"`
	Cases6069  int `json:"cases_60_69"`
	Cases7079  int `json:"cases_70_79"`
	Cases80    int `json:"cases_80"`
}

type FormattedReport struct {
	Date  string      `json:"date"`
	State string      `json:"state"`
	Cases ReportCases `json:"cases"`
	Sum   int         `json:"sum"`
}
