package report

import (
	"github.com/jinzhu/gorm"
)

type ReportRepository struct {
	database *gorm.DB
}

func (repository *ReportRepository) FindFilter(state, start, end string) ([]Report, error) {
	var reports []Report
	err := repository.database.Limit(100).Where("state = ?", state).
		Where("date BETWEEN ? AND ?", start, end).
		Find(&reports).Error
	if err != nil {
		return []Report{}, err
	}
	return reports, err
}

func (repository *ReportRepository) FindByState(state string) ([]Report, error) {
	var reports []Report
	err := repository.database.Limit(100).Where("state = ?", state).Find(&reports).Error
	return reports, err
}

func (repository *ReportRepository) FindInRange(start, end string) ([]Report, error) {
	var reports []Report
	err := repository.database.Limit(100).Where("date BETWEEN ? AND ?", start, end).Find(&reports).Error
	return reports, err
}

func (repository *ReportRepository) Create(report Report) (Report, error) {
	// this is a hack, ideally models.go should do this when unmarshalling
	// gocsv is acting up so ignoring
	report.State = CleanString(report.State).String()
	report.Date = CleanString(report.Date).String()

	err := repository.database.Create(&report).Error
	if err != nil {
		return report, err
	}

	return report, nil
}

func (repository *ReportRepository) Save(user Report) (Report, error) {
	err := repository.database.Save(user).Error
	return user, err
}

func NewReportRepository(database *gorm.DB) *ReportRepository {
	return &ReportRepository{
		database: database,
	}
}
