package report

import (
	"os"

	"github.com/gocarina/gocsv"
	"github.com/gofiber/fiber/v2"
	"github.com/jinzhu/gorm"
)

type ReportHandler struct {
	repository *ReportRepository
}

func (handler *ReportHandler) GetByState(c *fiber.Ctx) error {
	state := c.Query("state")
	reports, err := handler.repository.FindByState(state)

	if err != nil {
		return c.Status(404).JSON(fiber.Map{
			"status": 404,
			"error":  err,
		})
	}
	var formattedReport []FormattedReport
	for _, report := range reports {
		formattedReport = append(formattedReport, report.formatted())
	}
	return c.JSON(formattedReport)

}

func (handler *ReportHandler) GetByDateRange(c *fiber.Ctx) error {
	start := c.Query("startDate")
	end := c.Query("endDate")
	stateName := c.Query("state")

	// inclusive := c.Params("inclusive")
	var reports []Report
	var err error

	if stateName != "" {
		reports, err = handler.repository.FindFilter(stateName, start, end)
	} else {
		reports, err = handler.repository.FindInRange(start, end)

	}

	if err != nil {
		return c.Status(404).JSON(fiber.Map{
			"status": 404,
			"error":  err,
		})
	}
	var formattedReport []FormattedReport
	for _, report := range reports {
		formattedReport = append(formattedReport, report.formatted())
	}
	return c.JSON(formattedReport)
}

func (handler *ReportHandler) GetSum(c *fiber.Ctx) error {
	start := c.Params("startDate")
	end := c.Params("endDate")
	state := c.Params("name")
	report, err := handler.repository.FindFilter(state, start, end)

	if err != nil {
		return c.Status(404).JSON(fiber.Map{
			"status": 404,
			"error":  err,
		})
	}

	return c.JSON(report)
}

func (handler *ReportHandler) Upload(c *fiber.Ctx) error {

	// retrieve from /resource/uploads
	// readline as report and save
	uploadPath := "resource/uploads/data_covid.csv"
	file, err := os.OpenFile(uploadPath, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  400,
			"message": "Failed reading resource",
			"error":   err,
		})
	}
	defer file.Close()

	reports := []*Report{}
	if err := gocsv.UnmarshalFile(file, &reports); err != nil {
		return c.Status(400).JSON(fiber.Map{
			"status":  400,
			"message": "Malformed format",
			"error":   err,
		})
	}

	for _, report := range reports {
		_, err := handler.repository.Create(*report)
		if err != nil {
			return c.Status(400).JSON(fiber.Map{
				"status":  400,
				"message": "Failed creating item",
				"error":   err,
			})
		}
	}

	// move the file to archive
	archivePath := "resource/archives/data_covid.csv"
	err = os.Rename(uploadPath, archivePath)
	if err != nil {
		return c.Status(500).JSON(fiber.Map{
			"status":  500,
			"message": "failed moving files",
		})
	}

	return c.JSON(fiber.Map{
		"status":  200,
		"message": "OK",
	})
}

func NewReportHandler(repository *ReportRepository) *ReportHandler {
	return &ReportHandler{
		repository: repository,
	}
}

func Register(router fiber.Router, database *gorm.DB) {
	database.AutoMigrate(&Report{})
	reportRepository := NewReportRepository(database)
	reportHandler := NewReportHandler(reportRepository)

	router.Get("/by/date", reportHandler.GetByDateRange)
	router.Get("/by/state", reportHandler.GetByState)
	router.Get("/total", reportHandler.GetSum)

	// real use case will require handling file upload
	// move the file to uploads and then call ReportHandler.Create
	router.Put("/uploads", reportHandler.Upload)
}
